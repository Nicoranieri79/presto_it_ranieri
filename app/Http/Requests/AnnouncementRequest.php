<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnnouncementRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title'=>"required",
            'price'=>"required",
            'body'=>"required",
        ];
    }

    // public function messages(){
    //     return[
    //         "title.required"=>"Devi inserire il titolo",
    //         "price.required"=>"Devi inserire il prezzo",
    //         "body.required"=>"Devi inserire la descrizione",
    //     ];
    // }
}
