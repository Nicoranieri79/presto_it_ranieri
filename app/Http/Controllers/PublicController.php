<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function welcome() {
        $categories=Category::all();
        $announcements = Announcement::where("is_accepted", true)
        ->orderBy('created_at', 'desc')
        ->take(6)
        ->get();
        return view("welcome", compact("announcements", "categories"));
    }
    
    public function search(Request $request) {
        $q = $request->input("q");
        $announcements = Announcement::search($q)->get();
        return view("search.search_results", compact("q", "announcements"));
    }

    public function locale($locale) {
        session()->put("locale", $locale);
        return redirect()->back();
    }
}

