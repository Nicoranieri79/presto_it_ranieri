<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index($cat)
    {
        $ads=Announcement::where("category_id",$cat)
        ->where("is_accepted", true)
        ->orderBy("created_at", "desc")
        ->paginate(6);
        //->get();
        return view("category/selectCategory", compact("ads"));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Category $category)
    {
        //
    }

    public function edit(Category $category)
    {
        //
    }

    public function update(Request $request, Category $category)
    {
        //
    }

    public function destroy(Category $category)
    {
        //
    }
}
