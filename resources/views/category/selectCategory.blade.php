<x-layout>

    @foreach ($ads as $ad)
        <div class="card border-danger mb-3 mt-3" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">{{$ad->title}}</h5>
                <p class="card-text">{{$ad->price}} € </p>
                <p class="card-text">{{$ad->body}} </p>
                <p class="card-text">{{$ad->category->name}} </p>
                <p class="card-text">{{$ad->created_at->format('d/m/Y')}} </p>
            </div>
        </div>
    @endforeach
    
</x-layout>