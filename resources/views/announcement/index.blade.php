<x-layout>

    <div class="container">
        <div class="row justify-content-center">
            <h2 class="text-center mt-5 mb-3 fw-bold">{{ __('ui.announcements')}}</h2>
            @foreach ($announcements as $announcement)
            <div class="col-12 col-xl-4">
            <div class="text-center">
                    <article class="cardh linecard mx-3 my-3">
                        @foreach ($announcement->images as $image)
                        @if ($loop->first)
                              <img width="100%" height="100%" 
                              src="{{$image->getUrl(640, 360)}}" alt="">
                        @endif
                        @endforeach
                        <div class="card__info">
                            <h3 class="card__title">{{$announcement->title}}</h3>
                            <p class="card-text">{{ __('ui.price')}}: {{$announcement->price}} € </p>
                            <p class="card-text">{{$announcement->category->name}} </p>
                            <p class="card-text">{{$announcement->created_at->format('d/m/Y')}} </p>
                            <a href="{{route("detail" , compact("announcement") )}}" class="btn buttond linecard">{{ __('ui.details')}}</a>
                        </div>
                    </article>  
                </div>
            </div>
            @endforeach
        </div>
    </div>
    
</x-layout>