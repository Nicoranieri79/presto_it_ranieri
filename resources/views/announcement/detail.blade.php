<x-layout>

<div class="container ">
      <div class="row justify-content-center align-items-center mx-5" style="height:70vh;">
            <div class="col-12 col-md-8 col-lg-6">
                  <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                              @foreach($announcement->images as $image)
                                    @if ($loop->first)
                                          <div class="carousel-item active">
                                                <img width="100%" height="100%" src="{{$image->getUrl(640, 360)}}" alt="">
                                          </div>
                                    @endif
                                    @if ($loop->iteration != 1)
                                          <div class="carousel-item">
                                          <img width="100%" height="100%" src="{{$image->getUrl(640, 360)}}" alt="">
                                          </div>
                                    @endif
                              @endforeach
                        </div>
                                          <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                          <span class="visually-hidden">Previous</span>
                                          </button>
                                          <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                          <span class="visually-hidden">Next</span>
                                          </button>
                  </div>
            </div>
            <div class="col-12 col-md-8 col-lg-6">
                  <h1 class="text-center fw-bold">{{$announcement->title}}</h1>
                  <hr>
                  <div class="row">
                        <div class="deta">{{ __('ui.price')}} : {{$announcement->price}} € </div>
                        <div class="deta">{{ __('ui.category')}} : {{$announcement->category->name}} </div>
                        <div class="deta">{{ __('ui.description')}} : {{$announcement->body}} </div>
                        <hr>
                        <div class="deta">{{ __('ui.date')}} : {{$announcement->created_at->format('d/m/Y')}} </div>
                        <div class="deta">{{ __('ui.user')}} : {{$announcement->user->name}}</div>
                  </div>
            </div>
      </div>
</div>

</x-layout>