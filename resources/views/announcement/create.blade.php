<x-layout>

{{-- @if($errors->any()) 
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <div>{{$error}}</div>
            @endforeach
        </ul>
    </div>
@endif --}}

@if (session("announcement.created.success"))
    <div class="alert alert-success">
        {{ __('ui.successmessage')}}
    </div>
@endif
<div class="container-fluid">
    <div class="row justify-content-center">
        <h2 class="mt-5 mb-3 text-center fw-bold">{{ __('ui.postad')}}</h2>
        <div class="col-10 col-md-8 col-lg-6">
            {{-- <h3>DEBUG:: SECRET {{ $uniqueSecret }}</h3> --}}
            <form method="POST" action="{{route('store')}}">
                @csrf
                <div>
                    <input type="hidden" name="uniqueSecret" value="{{$uniqueSecret}}">
                </div>
                <div class="mb-3">
                    <label class="form-label">{{ __('ui.articlename')}} : </label>
                    <input type="text" class="form-control" name="title" placeholder="{{ __('ui.title')}}">
                    <br>
                    @error('title')
                    <div class="alert alert-danger">{{ __('ui.errorTitle')}}</div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label class="form-label">{{ __('ui.postPrice')}} : </label>
                    <input type="number" class="form-control" name="price" placeholder="{{ __('ui.price')}}">
                    <br>
                    @error('price')
                    <div class="alert alert-danger">{{ __('ui.errorPrize')}}</div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label class="form-label">{{ __('ui.postDescription')}} : </label>
                    <input type="text" class="form-control" name="body" placeholder="{{ __('ui.description')}}">
                    <br>
                    @error('body')
                    <div class="alert alert-danger">{{ __('ui.errorDescription')}}</div>
                    @enderror
                </div>
                <div class="mb-3">
                <label for="">{{ __('ui.choosecategory')}} : </label>
                <select name="category">
                    @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
                </div>
                
                <div class="form-group row">
                    <label for="images" class="col-md-12 col-form-label text-md-left">{{ __('ui.postImage')}} : </label>
                    <div class="col-md-12">
                        <div class="dropzone" id="drophere"></div>
                        @error('images')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                
                <button type="submit" class="btn buttonc mt-3">{{ __('ui.submit')}}</button>
            </form>
        </div>
    </div>
</div>

</x-layout>