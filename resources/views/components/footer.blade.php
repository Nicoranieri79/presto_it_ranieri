<footer class="bg-light text-center text-white">
  <div class="container p-4 pb-0">
      <section class="mb-4">
        <!-- Facebook -->
        <a class="btn linecard btn-floating m-1"
          style="background-color: #3b5998;"
          href="https://www.facebook.com/"
          role="button"
          ><i class="fab fa-facebook-f text-white"></i>
        </a>
        <!-- Twitter -->
        <a class="btn linecard btn-floating m-1"
          style="background-color: #55acee;"
          href="https://twitter.com/"
          role="button"
          ><i class="fab fa-twitter text-white"></i>
        </a>
        <!-- Google -->
        <a class="btn linecard btn-floating m-1"
          style="background-color: #dd4b39;"
          href="https://google.it/"
          role="button"
          ><i class="fab fa-google text-white"></i>
        </a>
        <!-- Instagram -->
        <a class="btn linecard btn-floating m-1"
          style="background-color: #ac2bac;"
          href="#!"
          role="button"
          ><i class="fab fa-instagram text-white"></i>
        </a>
        <!-- Linkedin -->
        <a class="btn linecard btn-floating m-1"
          style="background-color: #0082ca;"
          href="#!"
          role="button"
          ><i class="fab fa-linkedin-in text-white"></i>
        </a>
        <!-- Github -->
        <a class="btn linecard btn-floating m-1"
          style="background-color: #333333;"
          href="#!"
          role="button"
          ><i class="fab fa-github text-white"></i>
        </a>
      </section>
  </div>
  <div class="text-center p-3 text-dark" style="background-color: rgba(0, 0, 0, 0.2);">
    © 2022 Copyright <strong><span>T3amTree</span></strong>. All Rights Reserved
    <a class="text-white" href="https://aulab.it/"> Designed by Me medesimo</a>
  </div>
</footer>