<x-layout>

    @if($errors->any()) 
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <div>{{$error}}</div>
                @endforeach
            </ul>
        </div>
    @endif
    
<div class="container-fluid">
    <div class="row justify-content-center">
        <h2 class="mt-5 mb-3 text-center fw-bold">{{ __('ui.register')}}</h2>
        <div class="col-10 col-md-8 col-lg-6">
            <form method="POST" action="{{route('register')}}">
                @csrf
            <div class="mb-3">
                <label for="exampleInputUsername" class="form-label">{{ __('ui.username')}}</label>
                <input type="text" class="form-control" id="exampleInputUsername" name="name">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">{{ __('ui.email')}}</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" 
                    name="email">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">{{ __('ui.password')}}</label>
                <input type="password" class="form-control" id="exampleInputPassword1" name="password">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword2" class="form-label">{{ __('ui.conpassword')}}</label>
                <input type="password" class="form-control" id="exampleInputPassword2" name="password_confirmation">
            </div>
            <button type="submit" class="btn btn-primary">{{ __('ui.submit')}}</button>
            </form>
        </div>
    </div>
</div>

</x-layout>