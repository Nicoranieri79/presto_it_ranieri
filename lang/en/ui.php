<?php

return [
    // Homepage

    'welcome' => "Welcome to Presto.it",
    'categories' => "Categories",
    "show" => "Show",
    '6announcements' => "Last 6 announcements",
    "details" => "Go to details",
    "search"=>"Search",

    // Dettaglio annunci
    
    'category' => "Category",
    'date' => "Posted on",
    'user' => "Posted by",

    // Navbar

    'ourannouncements' => "Announcements",
    'insertannouncement' => "Post an announcement",
    "login" => "Login",
    'register' => "Sign in",
    "logout" => "Logout",
    'revisor' => "Revisor",

    // I nostri annunci

    'announcements' => 'Announcements',
    'title'=>"Title",
    'price'=>"Prize",
    'description'=>"Description",

    // Inserisci il tuo annuncio

    "announce"=>"Announce",
    "successmessage" => "Announcement created successfully! (awaiting review)",
    "postad" => "Post your announcement",
    "articlename" => "Insert the article name",
    "postPrice" => "Insert the price",
    "postDescription" => "Insert the description",
    "choosecategory" => "Choose category",
    "postImage"=>"Insert the image",

    // Revisore

    "revisorpagetitle" => "Revisors section",
    "accept" => "Accept",
    "refuse" => "Refuse",
    "noadstoreview" => "There is no announcements to review.",

    // Ricerca

    "searchresults" => "Results for:",

    // Registrazione

    "username"=>"Username",
    "email"=>"Email",
    "conemail"=>"Confirm email",
    "password"=>"Password",
    "conpassword"=>"Confirm password",
    "submit"=>"Submit",

    // Error

    "errorTitle"=>"Missing title",
    "errorPrize"=>"Missing prize",
    "errorDescription"=>"Missing description",
];