<?php

return [
    // Homepage

    'welcome' => "Benvenuti su Presto.it",
    'categories' => "Le nostre categorie",
    'show' => "Visualizza",
    '6announcements' => "Gli ultimi 6 annunci",
    'details' => "Vai al dettaglio",
    'search'=>"Cerca",

    // Dettaglio annunci

    'category' => "Categoria",
    'date' => "Pubblicato il",
    'user' => "Pubblicato da",
    
    // Navbar
    
    'ourannouncements' => "I nostri annunci",
    'insertannouncement' => "Inserisci annuncio",
    'login' => "Accedi",
    'register' => "Registrati",
    'logout' => "Logout",
    'revisor' => "Revisore",
    
    // I nostri annunci
    
    'announcements' => 'Annunci',
    'title'=>"Titolo",
    'price'=>"Prezzo",
    'description'=>"Descrizione",

    // Inserisci il tuo annuncio

    "announce"=>"Annuncio",
    "successmessage" => "Annuncio creato correttamente! (in attesa di revisione)",
    "postad" => "Inserisci il tuo annuncio",
    "articlename" => "Inserisci il nome dell'articolo",
    "postPrice" => "Inserisci il prezzo",
    "postDescription" => "Inserisci la descrizione",
    "choosecategory" => "Scegli la categoria",
    "postImage"=>"Inserisci l'mmagini",

    // Revisore

    "revisorpagetitle" => "Sezione dei revisori",
    "accept" => "Accetta",
    "refuse" => "Rifiuta",
    "noadstoreview" => "Non ci sono annunci da revisionare.",

    // Ricerca

    "searchresults" => "Risultati ricerca per:",

    // Registrazione

    "username"=>"Nome utente",
    "email"=>"Indirizzo email",
    "conemail"=>"Conferma indirizzo email",
    "password"=>"Password",
    "conpassword"=>"Conferma password",
    "submit"=>"Invia",

    // Error

    "errorTitle"=>"Titolo mancante",
    "errorPrize"=>"Prezzo mancante",
    "errorDescription"=>"Descrizione mancante",
];